package nl.hanze.web.t41.http;

import java.io.InputStream;
import java.io.OutputStream;

public interface HTTPHandler
{
    public void handleRequest(InputStream in, OutputStream out, String basisDirectory);
}
