package nl.hanze.web.t41.http;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HTTPRespons
{
    private final OutputStream out;
    private final String basisDirectory;
    private HTTPRequest request;
    private String contentLength;

    public HTTPRespons(OutputStream out, String basisDirectory)
    {
        this.out = out;
        this.basisDirectory = basisDirectory;
    }

    public void setRequest(HTTPRequest request)
    {
        this.request = request;
    }

    public void sendResponse() throws IOException
    {
        byte[] bytes = new byte[HTTPSettings.BUFFER_SIZE];
        InputStream inputStream = null;
        String requestMethod = request.getRequestMethod();
        String fileName = request.getUri();

        try
        {
            if (!fileName.isEmpty())
            {
                if (fileName.endsWith("/"))
                {
                    fileName = fileName.substring(0 ,fileName.length() - 1);
                }
                
                File file = new File(basisDirectory, fileName);
                
                inputStream = getInputStream(file, requestMethod);

                out.write(getHTTPHeader(fileName, requestMethod));

                int ch = inputStream.read(bytes, 0, HTTPSettings.BUFFER_SIZE);
                while (ch != -1)
                {
                    out.write(bytes, 0, ch);
                    ch = inputStream.read(bytes, 0, HTTPSettings.BUFFER_SIZE);
                }
            }
        }
        catch (IOException ex)
        {
            System.err.println(ex.getLocalizedMessage());
        }
        finally
        {
            if (inputStream != null)
            {
                inputStream.close();
            }
            if (out != null)
            {
                out.flush();
                out.close();
            }
        }
    }

    private InputStream getInputStream (File file, String requestMethod) throws FileNotFoundException
    {		
        InputStream fis;
        
        /*
          *** OPGAVE 4: 1b ***
          Stuur het bestand terug wanneer het bestaat; anders het standaard 404-bestand.
        */
        if (requestMethod.equals("GET"))
        {
            if (file.isDirectory())
            {
                File index = new File(file, "/index.html");
                if (index.exists())
                {
                    file = index;
                }
            }

            if (file.exists() && !file.isDirectory())
            {
                if (!isBestandsTypeOndersteund(request.getFileExt()))
                {
                    byte[] errorBA = HTTPCustomPage.getErrorPage(HTTPSettings.STATUS_CODE_500, HTTPSettings.STATUS_DESCRIPTION_500, request).getBytes();
                    contentLength = Integer.toString(errorBA.length);

                    fis = new ByteArrayInputStream(errorBA);
                }
                else
                {
                    contentLength = Long.toString(file.length());
                    fis = new FileInputStream(file);
                }
            }
            else if (file.exists() && file.isDirectory())
            {
                byte[] dirListingBA = HTTPCustomPage.getDirectoryListing(request, file.listFiles()).getBytes();
                contentLength = Integer.toString(dirListingBA.length);
                fis = new ByteArrayInputStream(dirListingBA);
            }
            else
            {
                byte[] errorBA = HTTPCustomPage.getErrorPage(HTTPSettings.STATUS_CODE_404, HTTPSettings.STATUS_DESCRIPTION_404, request).getBytes();
                contentLength = Integer.toString(errorBA.length);

                fis = new ByteArrayInputStream(errorBA);
            }
        }
        else
        {
            byte[] errorBA = HTTPCustomPage.getErrorPage(HTTPSettings.STATUS_CODE_405, HTTPSettings.STATUS_DESCRIPTION_405, request).getBytes();
            contentLength = Integer.toString(errorBA.length);

            fis = new ByteArrayInputStream(errorBA);
        }

        return fis;
    }

    private byte[] getHTTPHeader(String fileName, String requestMethod) throws IOException
    {
        /*
          ***  OPGAVE 4: 1b, 1c ***
          Alleen GET-requests, als er een andere request komt geeft de server een 405. Als het bestand niet gevonden wordt geeft de server een 404.
          Als de bestandstype niet wordt ondersteund geeft de server een 500. Verder wordt Allow, Content-Length, Content-Type, Date, Server en Status gezet.
        */
        int httpStatusCode;
        String httpStatusDescription;
        File file = new File(basisDirectory, fileName);
        StringBuilder header = new StringBuilder();
        
        if (!requestMethod.equals("GET"))
        {
            httpStatusCode = HTTPSettings.STATUS_CODE_405;
            httpStatusDescription = HTTPSettings.STATUS_DESCRIPTION_405;
        }
        else if (!file.exists())
        {
            httpStatusCode = HTTPSettings.STATUS_CODE_404;
            httpStatusDescription = HTTPSettings.STATUS_DESCRIPTION_404;
        }
        else if (!isBestandsTypeOndersteund(request.getFileExt()))
        {
            httpStatusCode = HTTPSettings.STATUS_CODE_500;
            httpStatusDescription = HTTPSettings.STATUS_DESCRIPTION_500;
        }
        else 
        {
            httpStatusCode = HTTPSettings.STATUS_CODE_200;
            httpStatusDescription = HTTPSettings.STATUS_DESCRIPTION_200;
        }
        header.append("HTTP/1.1 ");
        header.append(httpStatusCode);
        header.append(" ");
        header.append(httpStatusDescription);
        header.append("\r\n");
        
        header.append("Allow: GET");
        header.append("\r\n");
        
        header.append("Content-Length: ");
        header.append(contentLength);
        header.append("\r\n");
        
        header.append("Content-Type: ");
        header.append(HTTPSettings.getFileType(request.getFileExt()));
        header.append("\r\n");
        
        header.append("Date: ");
        header.append(HTTPSettings.getDate());
        header.append("\r\n");
        
        header.append("Server: ");
        header.append(HTTPSettings.SERVER);
        header.append(" (");
        header.append(HTTPSettings.OS);
        header.append(")");
        header.append("\r\n");
        
        header.append("Status: ");
        header.append(httpStatusCode);
        header.append(" ");
        header.append(httpStatusDescription);
        header.append("\r\n");
        
        header.append("\r\n");
        
        System.out.println("Response header:");
        System.out.println(header.toString());

        byte[] rv = header.toString().getBytes();
        return rv;
    }
    
    private boolean isBestandsTypeOndersteund(String fileExt)
    {
        switch (fileExt.toLowerCase())
        {
            case ".html":
                return true;
            case ".css":
                return true;
            case ".ico":
                return true;
            case ".gif":
                return true;
            case ".png":
                return true;
            case ".jpeg":
                return true;
            case ".jpg":
                return true;
            case ".js":
                return true;
            case ".pdf":
                return true;
            case ".txt":
                return true;
            default:
                return false;
        }
    }

    private void showResponse(byte[] respons)
    {
        StringBuilder buf = new StringBuilder(HTTPSettings.BUFFER_SIZE);

        for (int i = 0; i < respons.length; i++)
        {
            buf.append((char) respons[i]);
        }
    }
}
