package nl.hanze.web.t41.http;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public final class HTTPSettings
{
    /*
      *** OPGAVE 4: 1a ***
      Zet hier de juiste directory waar je bestanden staan.
    */
    public static final String DOC_ROOT = System.getProperty("user.home") + System.getProperty("file.separator") + "httpserver";

    public static final int BUFFER_SIZE = 2048;	
    public static final int PORT_MIN = 0;
    public static final int PORT_MAX = 65535;

    public static final String HOST = "localhost";
    public static final int PORT_NUM = 4444;
    public static final HashMap<String, String> dataTypes = new HashMap<>();	

    public static final String[] DAYS = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    
    public static final String SERVER = "Cookie Monster/3.0";
    public static final String OS = System.getProperty("os.name");
    
    public static final int STATUS_CODE_200 = 200;
    public static final int STATUS_CODE_404 = 404;
    public static final int STATUS_CODE_405 = 405;
    public static final int STATUS_CODE_500 = 500;
    
    public static final String STATUS_DESCRIPTION_200 = "OK";
    public static final String STATUS_DESCRIPTION_404 = "Not Found";
    public static final String STATUS_DESCRIPTION_405 = "Method Not Allowed";
    public static final String STATUS_DESCRIPTION_500 = "Internal Server Error";
    
    public static String getDate()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        
        return sdf.format(new Date());
    }
    
    public static String getFileType(String fileExt)
    {
        switch (fileExt.toLowerCase())
        {
            case ".css":
                return "text/css";
            case ".ico":
                return "image/x-icon";
            case ".gif":
                return "image/gif";
            case ".png":
                return "image/png";
            case ".jpeg":
                return "image/jpeg";
            case ".jpg":
                return "image/jpeg";
            case ".js":
                return "application/javascript";
            case ".pdf":
                return "application/pdf";
            case ".txt":
                return "text/plain";
            default:
                return "text/html";
        }
    }
}
