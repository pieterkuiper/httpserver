package nl.hanze.web.t41.http;

import java.io.File;
import java.text.SimpleDateFormat;

/**
 *
 * @author Pieter
 */
public class HTTPCustomPage
{
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    
    public static String getDirectoryListing(HTTPRequest request, File[] list)
    {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("<!DOCTYPE html>");
        strBuilder.append("<html>");
        strBuilder.append("<head>");
        strBuilder.append("<title>");
        strBuilder.append("Index van ");
        strBuilder.append(request.getUri());
        strBuilder.append("</title>");
        strBuilder.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        strBuilder.append("</head>");
        strBuilder.append("<body>");
        strBuilder.append("<h1>");
        strBuilder.append("Index van ");
        strBuilder.append(request.getUri());
        strBuilder.append("</h1>");
        strBuilder.append("<table>");
        strBuilder.append("<tr>");
        strBuilder.append("<th><img src=\"/icons/blank.gif\" alt=\"[ICO]\"></th>");
        strBuilder.append("<th>Naam</th>");
        strBuilder.append("<th>Bijgewerkt op</th>");
        strBuilder.append("<th>Grootte</th>");
        strBuilder.append("</tr>");
        strBuilder.append("<tr>");
        strBuilder.append("<th colspan=\"4\"><hr></th>");
        strBuilder.append("</tr>");
        strBuilder.append("<tr>");
        if (!request.getUri().equals("/"))
        {
            strBuilder.append("<td valign=\"top\"><img src=\"/icons/back.gif\" alt=\"[DIR]\"></td>");
            strBuilder.append("<td><a href=\"/\">Hoofddirectory</a></td>");
            strBuilder.append("<td>&nbsp;</td>");
            strBuilder.append("<td align=\"right\">-</td>");
            strBuilder.append("</tr>");
        }
        if (list.length == 0)
        {
            strBuilder.append("<tr>");
            strBuilder.append("<td colspan=\"4\">Deze map is leeg</td>");
            strBuilder.append("</tr>");
        }
        else
        {
            for (File file : list)
            {
                if (file.isDirectory())
                {
                    strBuilder.append("<tr>");
                    strBuilder.append("<td valign=\"top\"><img src=\"/icons/folder.gif\" alt=\"[DIR]\"></td>");
                    strBuilder.append("<td><a href=\"");
                    strBuilder.append(file.getName());
                    strBuilder.append("/\">");
                    strBuilder.append(file.getName());
                    strBuilder.append("/</a></td>");
                    strBuilder.append("<td align=\"right\">");
                    strBuilder.append(sdf.format(file.lastModified()));
                    strBuilder.append("</td>");
                    strBuilder.append("<td align=\"right\">-</td>");
                    strBuilder.append("</tr>");
                    
                }
                else
                {
                    strBuilder.append("<tr>");
                    strBuilder.append("<td valign=\"top\"><img src=\"/icons/file.gif\" alt=\"[FILE]\"></td>");
                    strBuilder.append("<td><a href=\"");
                    strBuilder.append(file.getName());
                    strBuilder.append("\">");
                    strBuilder.append(file.getName());
                    strBuilder.append("</a></td>");
                    strBuilder.append("<td align=\"right\">");
                    strBuilder.append(sdf.format(file.lastModified()));
                    strBuilder.append("</td>");
                    strBuilder.append("<td align=\"right\">");
                    strBuilder.append(file.length());
                    strBuilder.append("</td>");
                    strBuilder.append("</tr>");
                }
            }
        }
        strBuilder.append("<tr>");
        strBuilder.append("<th colspan=\"4\"><hr></th>");
        strBuilder.append("</tr>");
        strBuilder.append("</table>");
        strBuilder.append("<address>");
        strBuilder.append(HTTPSettings.SERVER);
        strBuilder.append(" (");
        strBuilder.append(HTTPSettings.OS);
        strBuilder.append(") Server op ");
        strBuilder.append(request.getHost());
        strBuilder.append(" Poort ");
        strBuilder.append(request.getPoort());
        strBuilder.append("</body>");
        strBuilder.append("</body>");
        strBuilder.append("</html>");
        
        return strBuilder.toString();
    }
    
    public static String getErrorPage(int httpStatusCode, String httpStatusDescription, HTTPRequest request)
    {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("<!DOCTYPE html>");
        strBuilder.append("<html>");
        strBuilder.append("<head>");
        strBuilder.append("<title>");
        strBuilder.append(httpStatusCode);
        strBuilder.append(" ");
        strBuilder.append(httpStatusDescription);
        strBuilder.append("</title>");
        strBuilder.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        strBuilder.append("</head>");
        strBuilder.append("<body>");
        strBuilder.append("<h1>");
        strBuilder.append(httpStatusCode);
        strBuilder.append(" ");
        strBuilder.append(httpStatusDescription);
        strBuilder.append("</h1>");
        switch (httpStatusCode)
        {
            case HTTPSettings.STATUS_CODE_404:
                strBuilder.append("<p>De gevraagde URL ");
                strBuilder.append(request.getUri());
                strBuilder.append(" werd niet gevonden op deze server.</p>");
                break;
            case HTTPSettings.STATUS_CODE_405:
                strBuilder.append("<p>De methode ");
                strBuilder.append(request.getRequestMethod());
                strBuilder.append(" is niet toegestaan op deze server.</p>");
                break;
            case HTTPSettings.STATUS_CODE_500:
                strBuilder.append("<p>Bestandstype ");
                strBuilder.append(request.getFileExt());
                strBuilder.append(" wordt niet onderteund op deze server.</p>");
                break;
        }
        strBuilder.append("<hr>");
        strBuilder.append("<address>");
        strBuilder.append(HTTPSettings.SERVER);
        strBuilder.append(" (");
        strBuilder.append(HTTPSettings.OS);
        strBuilder.append(") Server op ");
        strBuilder.append(request.getHost());
        strBuilder.append(" Poort ");
        strBuilder.append(request.getPoort());
        strBuilder.append("</body>");
        strBuilder.append("</body>");
        strBuilder.append("</html>");
        
        
        return strBuilder.toString();
    }
}
