package nl.hanze.web.t41.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;

public class HTTPHandlerImpl implements HTTPHandler
{
    @Override
    public void handleRequest(InputStream in, OutputStream out, String basisDirectory)
    {
        HTTPRequest request = new HTTPRequest(in, basisDirectory);
        HTTPRespons respons = new HTTPRespons(out, basisDirectory);	

        request.setRequestHeader();
        respons.setRequest(request);

        showDateAndTime();
        System.out.println(": " + request.getUri());

        try
        {
            respons.sendResponse();			
        }
        catch (IOException ex)
        {
            System.err.println(ex.getLocalizedMessage());
        }
    }

    private void showDateAndTime ()
    {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        System.out.print(format.format(new Date()));
    }
}
