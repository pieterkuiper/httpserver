package nl.hanze.web.t41.http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HTTPListener implements Runnable
{
    private Thread thread;
    private int portnumber;
    private String basisDirectory;
    private HTTPHandler httpHandler;
    
    public HTTPListener(int port, String basisDirectory, HTTPHandler hh) throws Exception
    {
        if (port < HTTPSettings.PORT_MIN || port > HTTPSettings.PORT_MAX)
        {
            throw new Exception("Invalid TCP/IP port, out of range");
        }
        this.portnumber = port;
        this.basisDirectory = basisDirectory;
        this.httpHandler = hh;
    }
	
    public void startServer()
    {
        thread = new Thread(this);
        thread.start();
    }
    
    public void stopServer()
    {        
        if (thread != null && thread.isAlive())
        {
            thread.interrupt();
        }
        thread = null;
    }

    @Override
    public void run()
    {
        try
        {
            ServerSocket servsock = new ServerSocket(portnumber);
            System.out.println("Server started");
            System.out.println("Waiting requests at port " + portnumber);
            
            while (true)
            {
                try (Socket socket = servsock.accept())
                {
                    httpHandler.handleRequest(socket.getInputStream(), socket.getOutputStream(), basisDirectory);
                }
            }
        }
        catch (IOException ex)
        {
            System.err.println(ex.getLocalizedMessage());
        }
    }
}
