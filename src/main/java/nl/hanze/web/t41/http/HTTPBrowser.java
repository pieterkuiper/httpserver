package nl.hanze.web.t41.http;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author Pieter
 */
public class HTTPBrowser
{
    public static void startBrowser(int portNumber)
    {
        String url = "http://" + HTTPSettings.HOST + ":" + portNumber;

        if (Desktop.isDesktopSupported())
        {
            Desktop desktop = Desktop.getDesktop();
            try
            {
                desktop.browse(new URI(url));
            }
            catch (IOException | URISyntaxException ex)
            {
                System.err.println(ex.getLocalizedMessage());
            }
        }
        else
        {
            Runtime runtime = Runtime.getRuntime();
            try
            {
                runtime.exec("xdg-open " + url);
            }
            catch (IOException ex)
            {
                System.err.println(ex.getLocalizedMessage());
            }
        }
    }
}
