package nl.hanze.web.t41.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class HTTPRequest
{
    private String requestMethod = "";
    private String uri = "";
    private String fileExt = "";
    private String host = "";
    private String poort = "";
    private final InputStream in;
    private final String basisDirectory;

    public HTTPRequest(InputStream in, String basisDirectory)
    {
        this.in = in;
        this.basisDirectory = basisDirectory;
    }

    public String getRequestMethod()
    {
        return requestMethod;
    }

    public String getUri()
    {
        return uri;
    }

    public String getFileExt()
    {
        return fileExt;
    }

    public String getHost()
    {
        return host;
    }

    public String getPoort()
    {
        if (poort.equals(""))
        {
            return "80";
        }
        return poort;
    }
    
    public void setRequestHeader()
    {
        String request = parseRequest();
        String[] regels = request.split(System.getProperty("line.separator"));
        if (regels.length > 0)
        {
            String[] requestRegelArray = regels[0].split(" ");
            
            if (requestRegelArray.length > 0)
            {
                requestMethod = requestRegelArray[0];
            }
            if (requestRegelArray.length > 1)
            {
                uri = requestRegelArray[1].replace("%20", " ");
                fileExt = getFileExt(uri);
            }
            
            for (String regel : regels)
            {
                String[] regelArray = regel.split(": ");

                if (regelArray.length > 0)
                {
                    if (regelArray[0].equals("Host"))
                    {
                        String[] hostArray = regel.split(":");
                        
                        if (hostArray.length > 1)
                        {
                            host = hostArray[1];
                        }
                        if (hostArray.length > 2)
                        {
                            poort = hostArray[2];
                        }
                    }
                }
            }
        }
    }

    private String parseRequest()
    {
        StringBuilder request = new StringBuilder(HTTPSettings.BUFFER_SIZE);
        int i;
        byte[] buffer = new byte[HTTPSettings.BUFFER_SIZE];

        try
        {
            i = in.read(buffer);
        }
        catch (IOException ex)
        {
            System.err.println(ex.getLocalizedMessage());
            i = -1;
        }

        for (int j = 0; j < i; j++)
        {
            request.append((char) buffer[j]);
        }
        
        System.out.println("Request header:");
        System.out.println(request.toString());

        return request.toString();
    }
    
    private String getFileExt(String uri)
    {
        if (new File(basisDirectory, uri).isDirectory())
        {
            return ".html";
        }
        
        if (uri.contains("."))
        {
            return uri.substring(uri.lastIndexOf('.'), uri.length());
        }
        else
        {
            return "";
        }
    }
}
