package nl.hanze.web.t41.runner;

import java.io.File;
import nl.hanze.web.t41.http.HTTPBrowser;
import nl.hanze.web.t41.http.HTTPHandlerImpl;
import nl.hanze.web.t41.http.HTTPListener;
import nl.hanze.web.t41.http.HTTPSettings;

public class HTTPRunner 
{
    public static void main(String args[])
    {
        /* 
          *** OPGAVE 1.1 ***
          zorg ervoor dat het port-nummer en de basis-directory vanuit de command-line kunnen worden meegegeven.
          LET OP: de default-waarden moet je nog wel instellen in de Settings-klasse.
        */
        int portNumber = HTTPSettings.PORT_NUM;
        String basisDirectory = HTTPSettings.DOC_ROOT;

        if (args.length != 0 && args.length != 2)
        {
            System.err.println("U moet 0 of 2 parameters meegeven.");
            System.exit(0);
        }
        else if (args.length == 2)
        {
            try
            {
                portNumber = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException ex)
            {
                System.err.println("De eerste parameter moet een getal zijn");
                System.err.println(args[0] + " is geen getal");
                System.exit(0);
            }
            
            basisDirectory = args[1];
            
            File file = new File(basisDirectory);
            
            if (!file.isDirectory())
            {
                System.err.println("De tweede parameter moet bestaande directory zijn");
                System.err.println(args[1] + " is geen bestaande directory");
                System.exit(0);
            }
        }
		
        try
        {
            HTTPListener listener = new HTTPListener(portNumber, basisDirectory, new HTTPHandlerImpl());
            listener.startServer();
            HTTPBrowser.startBrowser(portNumber);
        }
        catch (Exception ex)
        {
            System.err.println(ex.getLocalizedMessage());
        }
    }
}
